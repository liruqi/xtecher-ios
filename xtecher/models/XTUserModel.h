//
//  XTUserModel.h
//  xtecher
//
//  Created by liruqi on 5/19/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "JSONModel.h"

@protocol XTUserModel
@end

@interface XTUserModel : JSONModel

@property (strong, nonatomic) NSString *uid;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *job_title;
@property (strong, nonatomic) NSString *company_name;
@property (strong, nonatomic) NSString *avator;

- (NSString*) title;

@end

