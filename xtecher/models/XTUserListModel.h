//
//  XTUserListModel.h
//  xtecher
//
//  Created by liruqi on 5/19/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "JSONModel.h"
#import "XTUserModel.h"

@interface XTUserListModel : JSONModel

@property(nonatomic) int status;
@property(strong,nonatomic) NSArray<XTUserModel> *data;

@end
