//
//  XTUserModel.m
//  xtecher
//
//  Created by liruqi on 5/19/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTUserModel.h"

@implementation XTUserModel

- (NSString*) title {
    if (_company_name.length > 0) {
        NSString* t = _job_title.length > 0 ? _job_title : @"Staff";
        return [NSString stringWithFormat:@"%@ @ %@", t, _company_name];
    }
    return _job_title;
}

@end
