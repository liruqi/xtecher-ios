//
//  FirstViewController.m
//  xtecher
//
//  Created by liruqi on 5/2/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTFirstViewController.h"
#import "XTUserPreference.h"
#import "XTWebPageViewController.h"

@interface XTFirstViewController ()
{
    UITabBarItem *_tabBarItem;
}
@end

@implementation XTFirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self reset];
    self.title = @"Home";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITabBarItem*) tabBarItem {
    if (! _tabBarItem) {
        _tabBarItem = [[UITabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"tab_home"] tag:0];
        _tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    }
    return _tabBarItem;
}

- (void) reset {
    XTUser *user = [XTUserPreference currentUser];
    NSString *uri = [NSString stringWithFormat:@"%@%@?token=%@&client=ios", XT_BASEURL, XT_HOME_PATH, user.token];
    if ([uri isEqual: [self.webView stringByEvaluatingJavaScriptFromString:@"window.location.href"]]) {
        return;
    }
    NSURL *url = [NSURL URLWithString:uri];
    [self.webView loadRequest:[NSURLRequest requestWithURL: url]];
}

- (BOOL) shouldStartLoad: (NSURL*) url {
    NSLog(@"shouldStartLoad: %@", url);
    if ([url.path isEqualToString:XT_PROJECT_VIEW_PATH] ||
        [url.path isEqualToString:XT_PROFILE_VIEW_PATH] ||
        [url.path isEqualToString:XT_ARTICLE_VIEW_PATH]) {
        XTWebPageViewController* postVC = [[XTWebPageViewController alloc] initWithURL: url];
        postVC.barHidden = YES;
        [self.navigationController pushViewController:postVC animated:YES];
        return NO;
    }
    
    return YES;
}
@end
