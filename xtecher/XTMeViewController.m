//
//  XTMeViewController.m
//  xtecher
//
//  Created by liruqi on 5/10/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTMeViewController.h"
#import "XTUserPreference.h"
#import "SVProgressHUD.h"

@interface XTMeViewController () <UIActionSheetDelegate>

@end

@implementation XTMeViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"Home";
    [self reset];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reset];
}

- (UITabBarItem*) tabBarItem {
    UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"tab_me"] tag:3];
    item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    return item;
}

- (void) reset {
    XTUser *user = [XTUserPreference currentUser];
    NSString *uri = [NSString stringWithFormat:@"http://xtecher.com/Wap/Profile/my?token=%@&client=ios", user.token];
    if ([uri isEqual: [self.webView stringByEvaluatingJavaScriptFromString:@"window.location.href"]]) {
        return;
    }
    NSURL *url = [NSURL URLWithString:uri];
    [self.webView loadRequest:[NSURLRequest requestWithURL: url]];
}

- (void) xtecherUploadAvatar {
    NSLog(@"xtecherUploadAvatar");
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:@"Cancel" // 取消
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Take Photo", @"Choose from Photos", nil]; //@"照相机", @"本地相簿",
    [actionSheet showInView:self.view];
}
#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0://照相机
        {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.allowsEditing = YES;
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
            break;
            
        case 1://本地相簿
        {
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            imagePicker.delegate = self;
            imagePicker.allowsEditing = YES;
            imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
}


#pragma mark -
#pragma UIImagePickerController Delegate
//当选择一张图片后进入这里
-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info

{
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"])
    {
        //先把图片转成NSData
        UIImage* image = [info objectForKey:UIImagePickerControllerEditedImage];
        
//        NSData *_data = UIImagePNGRepresentation(image);
//        
//        if (_data == nil)
//        {
//            _data = UIImageJPEGRepresentation(image, 1.0);
//        }
        
        //关闭相册界面
        [picker dismissViewControllerAnimated:YES completion:^{}];
        
        [self uploadAvatarWithData: UIImageJPEGRepresentation(image, 1)];
    }
    
}

- (void) uploadAvatarWithData: (NSData*) data {
    
    [SVProgressHUD show];
    AFHTTPRequestOperationManager *manager = [XTUserPreference sharedInstance].requestManager;
    XTUser *user = [XTUserPreference currentUser];
    NSDictionary* params = @{ @"token" : user.token};
    [manager                POST:@"http://xtecher.com/Wap/User/uploadAvatar?client=ios&lang=en"
                      parameters:params
       constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
           [formData appendPartWithFileData:data name:@"avatar" fileName:@"avatar.jpg" mimeType:@"image/jpeg"];
           //[formData appendPartWithFormData:data name:@"avatar"];
       } success:^(AFHTTPRequestOperation *operation, id responseObject) {
           NSLog(@"Success: %@", responseObject);
           [SVProgressHUD dismiss];
           [self.webView reload];
       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           NSLog(@"Error: %@", error);
           NSString* msg = [NSString stringWithFormat:@"Upload error: %@", error];
           [SVProgressHUD showErrorWithStatus: msg];
       }
    ];
}


@end
