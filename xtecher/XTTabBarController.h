//
//  XTTabBarController.h
//  xtecher
//
//  Created by liruqi on 5/2/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface XTTabBarController : UITabBarController
{
    EMConnectionState _connectionState;
}

- (void)jumpToChatList;

- (void)networkChanged:(EMConnectionState)connectionState;

@end
