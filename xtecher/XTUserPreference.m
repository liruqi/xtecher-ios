//
//  XTUserPreference.m
//  xtecher
//
//  Created by liruqi on 5/2/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTUserPreference.h"
#import "JSONHTTPClient.h"
#import "XTUserListModel.h"
#import "AFHTTPRequestOperationManager.h"

@implementation XTUser

- (instancetype) initWithUid: (NSString* )uid path:(NSArray*) a  {
    self = [super init];
    if (self) {
        self.uid = uid;
        self.name = a[1];
        self.token = a[2];
        self.hxpasswd = a[3];
    }
    return self;
}

- (void) save {
    
}

+ (instancetype) fromURL:(NSURL *)url {
    NSArray *a = [url.path pathComponents];
    if (a.count == 4) {
        return [[XTUser alloc] initWithUid:url.host path: a];
    }
    return nil;
}

@end

static XTUser* sCurrentUser;

@implementation XTUserPreference

+ (NSUserDefaults *) getGloabalPreference {
    return [NSUserDefaults standardUserDefaults];
}

+ (XTUser *) currentUser {
    if (! sCurrentUser) {
        NSUserDefaults* gloabalPref = [self getGloabalPreference];
        
        NSString * jsonString = [gloabalPref stringForKey:@"CurrentUser"];
        if (jsonString.length) {
            NSError* e;
            sCurrentUser = [[XTUser alloc] initWithString:jsonString error:&e];
            NSLog(@"Get current user: %@ error: %@", sCurrentUser, e);
        }
    }
    return sCurrentUser;
}

+ (void) setCurrentUser:(XTUser *)userInfo {
    sCurrentUser = userInfo;
    NSUserDefaults* pref = [self getGloabalPreference];
    
    if (userInfo == nil) {
        [pref removeObjectForKey:@"CurrentUser"];
        return;
    }
    [pref setObject:userInfo.toJSONString forKey:@"CurrentUser"];
}

+ (void)clearCurrentUser
{
    [self setCurrentUser:nil];
}

+ (instancetype) sharedInstance {
    static dispatch_once_t onceToken;
    static XTUserPreference* p;
    dispatch_once(&onceToken, ^{
        p = [XTUserPreference new];
        p.contactsMap = [NSMapTable mapTableWithKeyOptions:NSMapTableStrongMemory
                                              valueOptions:NSMapTableStrongMemory];
        p.requestManager = [AFHTTPRequestOperationManager manager];
    });
    return p;
}

- (void) updateContacts: (NSArray*) c {
    [c enumerateObjectsUsingBlock:^(XTUserModel *u, NSUInteger idx, BOOL *stop ) {
        [self.contactsMap setObject:u forKey:u.uid];
    }];
}

- (XTUserModel*) contactByID: (NSString*) uid {
    return [self.contactsMap objectForKey:uid];
}

- (void) requestContacts: (NSArray*) uids withBlock: (void(^)(JSONModelError *err)) block {
    if (uids.count == 0) {
        return;
    }
    XTUser *user = [XTUserPreference currentUser];
    NSDictionary* params = @{
                             @"token" : user.token,
                             @"uids": [uids  componentsJoinedByString: @","]};
    [JSONHTTPClient getJSONFromURLWithString:@"http://xtecher.com/Wap/Client/getUserInfo"
                                      params:params
                                  completion:^(id json, JSONModelError *err) {
                                      if (err) {
                                          NSLog(@"getUserInfo error: %@", err);
                                      } else {
                                          XTUserListModel* response = [[XTUserListModel alloc] initWithDictionary: json error: &err];
                                          if (response.status == 1) {
                                              [self updateContacts:response.data];
                                          }
                                      }
                                      block(err);
                                  }];
}

- (void) requestFriendsListWithBlock: (void(^)(NSArray* respones, NSError *err)) block {
    XTUser *user = [XTUserPreference currentUser];
    NSDictionary* params = @{@"token" : user.token};
    [JSONHTTPClient getJSONFromURLWithString:@"http://xtecher.com/Wap/Client/getFriendsList"
                                      params:params
                                  completion:^(id json, JSONModelError *err) {
                                      if (err) {
                                          NSLog(@"getUserInfo error: %@", err);
                                          block(nil, err);
                                      } else {
                                          XTUserListModel* response = [[XTUserListModel alloc] initWithDictionary: json error: &err];
                                          if (response.status == 1) {
                                              [self updateContacts:response.data];
                                          }
                                          block(response.data, err);
                                      }
                                      
                                  }];
}
@end
