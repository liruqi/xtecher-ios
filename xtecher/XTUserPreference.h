//
//  XTUserPreference.h
//  xtecher
//
//  Created by liruqi on 5/2/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "XTUserModel.h"
#import "XTUserListModel.h"
#import "AFHTTPRequestOperationManager.h"

@interface XTUser : JSONModel

@property(nonatomic, retain) NSString *uid;
@property(nonatomic, retain) NSString *name;
@property(nonatomic, retain) NSString *token;
@property(nonatomic, retain) NSString *hxpasswd;

+ (instancetype) fromURL: (NSURL*) url;

@end

@interface XTUserPreference : NSObject

@property(nonatomic, retain) NSMapTable *contactsMap;
@property(nonatomic, retain) AFHTTPRequestOperationManager *requestManager;

+ (XTUser *) currentUser;
+ (void) setCurrentUser:(XTUser *)user;
+ (void) clearCurrentUser;
+ (instancetype) sharedInstance;

- (void) updateContacts: (NSArray*) c;
- (XTUserModel*) contactByID: (NSString*) uid;
- (void) requestContacts: (NSArray*) uids withBlock: (void(^)(JSONModelError *err)) block;
- (void) requestFriendsListWithBlock: (void(^)(NSArray* friends, NSError *err)) block;
@end
