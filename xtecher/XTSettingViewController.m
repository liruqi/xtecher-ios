//
//  XTSettingViewController.m
//  xtecher
//
//  Created by liruqi on 5/27/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTSettingViewController.h"
#import "XTUserPreference.h"
#import "LoginViewController.h"
#import "XTAboutViewController.h"
const static NSInteger numberOfRows[] = {2,1};

@interface XTSettingViewController ()
{
    BOOL _navigationBarHidden;
    UIColor* _tintColor;
}
@end

@implementation XTSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //#e6ecf1
    self.view.backgroundColor = RGBACOLOR(0xe6, 0xec, 0xf1, 1);
    self.tableView.backgroundColor = RGBACOLOR(0xe6, 0xec, 0xf1, 1);
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    _navigationBarHidden = self.navigationController.navigationBarHidden;
    _tintColor = self.navigationController.navigationBar.tintColor;
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.title = NSLocalizedString(@"title.setting", @"Settings");
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBar.tintColor = _tintColor;
    self.navigationController.navigationBarHidden = _navigationBarHidden;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return sizeof(numberOfRows) / sizeof(NSInteger);
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //const static NSInteger numberOfRows[3] = {3,2,1};
    // Return the number of rows in the section.
    return numberOfRows[section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        const NSArray* titlesForSection1 = @[NSLocalizedString(@"About Xtecher", @"About Xtecher"), NSLocalizedString(@"Rate in App Store", @"Rate in App Store"), @"Recommend to friends"];
        
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.text = titlesForSection1[indexPath.row];
//        if (indexPath.row == 0) {
//            UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
//            label.text = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
//            cell.accessoryView = label;
//            //cell.detailTextLabel.text = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
//        }
        return cell;
    }
    /*
    if (indexPath.section == 1) {
        const NSArray* titlesForSection2 = @[@"Language", @"About Us"];
        
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:nil];
        cell.textLabel.text = titlesForSection2[indexPath.row];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }
    */
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    cell.textLabel.text = NSLocalizedString(@"logout", @"Log Out");
//    dispatch_async(dispatch_get_main_queue(), ^{
//        CGRect lastRowRect= [tableView rectForRowAtIndexPath:indexPath];
//        CGFloat contentHeight = lastRowRect.origin.y + lastRowRect.size.height;
//        CGRect frame = tableView.frame;
//        frame.size.height = contentHeight;
//        tableView.frame = frame;
//    });
    return cell;
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
//            NSString *iTunesLink = @"http://xtecher.com/";
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
            XTAboutViewController *aboutVC = [[XTAboutViewController alloc] init];
            [self.navigationController pushViewController:aboutVC animated:YES];
        }
        if (indexPath.row == 1) {
            NSString *iTunesLink = @"https://itunes.apple.com/us/app/xtecher-quan-qiu-ke-ji-she/id1001897088?&mt=8";
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        }
    }
    if (indexPath.section == 1) {
        __weak typeof(self) weakSelf = self;
        [self showHudInView:self.view hint:NSLocalizedString(@"setting.logoutOngoing", @"loging out...")];
        
        [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:YES completion:^(NSDictionary *info, EMError *error) {
            [weakSelf hideHud];
            if (error && error.errorCode != EMErrorServerNotLogin) {
                [weakSelf showHint:error.description];
            }
            else {
                [XTUserPreference setCurrentUser: nil];
                
//                UIViewController* rootVC = [weakSelf.navigationController.viewControllers firstObject];
                LoginViewController *loginVC = [[LoginViewController alloc] init];
                
//                [weakSelf.navigationController setViewControllers:@[rootVC, loginVC] animated:YES];
                [weakSelf.navigationController popViewControllerAnimated:NO];
                [weakSelf.navigationController pushViewController:loginVC animated:YES];

            }
        } onQueue:nil];
    }
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 16;
}

@end
