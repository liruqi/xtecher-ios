//
//  XTDiscoverViewController.m
//  xtecher
//
//  Created by liruqi on 5/10/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTDiscoverViewController.h"
#import "XTUserPreference.h"
#import "XTWebPageViewController.h"

static NSString* XT_CREATEPOST_URL = @"/Wap/Post/createPostPage?client=ios";

@interface XTDiscoverViewController ()

@end

@implementation XTDiscoverViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"Home";
    [self reset];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.webView.isLoading) {
        return;
    }
    [self.webView stringByEvaluatingJavaScriptFromString:@"ClientJS.refreshDiscover();"];
}

- (UITabBarItem*) tabBarItem {
    UITabBarItem *item = [[UITabBarItem alloc] initWithTitle:nil image:[UIImage imageNamed:@"tab_discover"] tag:1];
    item.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
    return item;
}

- (void) reset {
    XTUser *user = [XTUserPreference currentUser];
    NSString *uri = [NSString stringWithFormat:@"%@%@?token=%@&client=ios",XT_BASEURL, XT_DISCOVER_PATH, user.token];
    NSURL *url = [NSURL URLWithString:uri];
    [self.webView loadRequest:[NSURLRequest requestWithURL: url]];
}

- (BOOL) shouldStartLoad: (NSURL*) url {
    if ([url.path isEqualToString:XT_CREATEPOST_URL]) {
        XTWebPageViewController* postVC = [[XTWebPageViewController alloc] initWithURL: url];
        postVC.barHidden = YES;
        [self.navigationController pushViewController:postVC animated:YES];
        return NO;
    }
    
    if ([url.path isEqualToString:XT_PROJECT_VIEW_PATH] ||
        [url.path isEqualToString:XT_PROFILE_VIEW_PATH] ||
        [url.path isEqualToString:XT_ARTICLE_VIEW_PATH]) {
        XTWebPageViewController* postVC = [[XTWebPageViewController alloc] initWithURL: url];
        postVC.barHidden = YES;
        [self.navigationController pushViewController:postVC animated:YES];
        return NO;
    }
    
    return YES;
}
@end
