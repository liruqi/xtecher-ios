//
//  XTWebViewController.h
//  xtecher
//
//  Created by liruqi on 5/19/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "BaseViewController.h"
#import "XTWebView.h"

@interface XTWebViewController : BaseViewController<UIWebViewDelegate>

@property (retain, nonatomic) XTWebView *webView;

- (void) xtecherUploadAvatar;

- (BOOL) canGoBack;

- (void) goBack;

- (void) xtecherCommand: (NSURL*) url;

@end
