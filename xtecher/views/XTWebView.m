//
//  XTWebView.m
//  xtecher
//
//  Created by liruqi on 5/30/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTWebView.h"

@implementation XTWebView

- (BOOL) canGoBack {
    if (self.xtDelegate) {
        return [self.xtDelegate canGoBack];
    }
    return [super canGoBack];
}

- (void) goBack {
    if (self.xtDelegate) {
        [self.xtDelegate goBack];
        return;
    }
    [super goBack];
}

@end
