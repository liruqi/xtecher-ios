//
//  XTWebView.h
//  xtecher
//
//  Created by liruqi on 5/30/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol XTWebViewDelegate <NSObject>

- (BOOL) canGoBack;
- (void) goBack;

@end
@interface XTWebView : UIWebView

@property (nonatomic, assign) id<XTWebViewDelegate> xtDelegate;

@end
