//
//  XTCreatePostViewController.h
//  xtecher
//
//  Created by liruqi on 5/30/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTWebViewController.h"

@interface XTWebPageViewController : XTWebViewController

@property (nonatomic) BOOL barHidden;

-(instancetype) initWithURL: (NSURL*) url;

@end
