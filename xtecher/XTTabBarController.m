//
//  XTTabBarController.m
//  xtecher
//
//  Created by liruqi on 5/2/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTTabBarController.h"
#import "XTFirstViewController.h"
#import "XTChatViewController.h"
#import "XTUserPreference.h"
#import "XTDiscoverViewController.h"
#import "XTMeViewController.h"

#import "UIViewController+HUD.h"
#import "XTContactsViewController.h"
#import "ApplyViewController.h"
#import "CallViewController.h"
#import "SVProgressHUD.h"
#import "LoginViewController.h"
#import "JSONHTTPClient.h"
#import "InvitationManager.h"
#import "XTUserListModel.h"

//两次提示的默认间隔
static const CGFloat kDefaultPlaySoundInterval = 3.0;

@interface XTTabBarController () <UIAlertViewDelegate, IChatManagerDelegate, EMCallManagerDelegate>
{
    XTFirstViewController *_indexVC;
    XTDiscoverViewController *_discoverVC;
    XTChatViewController * _chatVC;
    XTMeViewController *_meVC;
    
    UIBarButtonItem *_friendsItem;
    UIBarButtonItem *_createPostItem;
    UIBarButtonItem *_editProfileItem;
}

@property (strong, nonatomic) NSDate *lastPlaySoundDate;

@end

@implementation XTTabBarController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //if 使tabBarController中管理的viewControllers都符合 UIRectEdgeNone
    if ([UIDevice currentDevice].systemVersion.floatValue >= 7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    self.title = NSLocalizedString(@"title.conversation", @"Conversations");
    
    //获取未读消息数，此时并没有把self注册为SDK的delegate，读取出的未读数是上次退出程序时的
    //    [self didUnreadMessagesCountChanged];
#warning 把self注册为SDK的delegate
    [self registerNotifications];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setupUntreatedApplyCount) name:@"setupUntreatedApplyCount" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callOutWithChatter:) name:@"callOutWithChatter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(callControllerClose:) name:@"callControllerClose" object:nil];
    
    [self setupSubviews];
    self.selectedIndex = 0;

    _friendsItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"following.png"] style:UIBarButtonItemStylePlain target:self action:@selector(onFriendsTapped:)];
    _createPostItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(onCreatePostTapped:)];

    [self setupUnreadMessageCount];
    self.tabBar.tintColor = [UIColor whiteColor];
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    XTUser *user = [XTUserPreference currentUser];
    if (user) {
        if (self.selectedViewController == _indexVC) {
            [self tabBar: self.tabBar didSelectItem:_indexVC.tabBarItem];
            //self.navigationController.navigationBarHidden = YES;
        }
        
        if (! [[EaseMob sharedInstance].chatManager isLoggedIn]) {
            if ([self isBeingPresented]) {
                [SVProgressHUD show];
                [[UIApplication sharedApplication] beginIgnoringInteractionEvents];
            }
            [[EaseMob sharedInstance].chatManager asyncLoginWithUsername:user.uid password:user.hxpasswd completion:^(NSDictionary *loginInfo, EMError *error) {
                if (!error && loginInfo) {
                    [self onLoggedIn];
                    NSLog(@"登陆成功: %@", loginInfo);
                } else {
                    NSLog(@"登陆失败: %@", error);
                }
                [SVProgressHUD dismiss];
                [[UIApplication sharedApplication] endIgnoringInteractionEvents];
            } onQueue: dispatch_get_main_queue()];
        } else {
            [self onLoggedIn];
        }
        
        //TODO: check token
    } else {
        [self onLogin];
    }
}

- (void) onLogin {
    LoginViewController* vc = [LoginViewController new];
    [self presentViewController:vc animated:YES completion:^(){
    }];
}

- (void) onLoggedIn {
    XTUser *user = [XTUserPreference currentUser];

    [[EaseMob sharedInstance].chatManager asyncFetchMyGroupsList];
    
    //设置是否自动登录
    // [[EaseMob sharedInstance].chatManager setIsAutoLoginEnabled:YES];
    
    [[EaseMob sharedInstance].chatManager loadDataFromDatabase];
    [[ApplyViewController shareController] loadDataSourceFromLocalDB];
    
    [self updateContactsInfo:@[user.uid]];
    [[XTUserPreference sharedInstance] requestFriendsListWithBlock:^(NSArray* friends, NSError* e) {
        if (e) {
            [SVProgressHUD showErrorWithStatus:@"获取好友列表失败"];
        }
    }];
}

- (void) updateContactsInfo :(NSArray*) uids {
    if (uids.count == 0) {
        return ;
    }

    XTUser *user = [XTUserPreference currentUser];
    NSDictionary* params = @{
                             @"token" : user.token,
                             @"uids": [uids componentsJoinedByString: @","]};
    [JSONHTTPClient getJSONFromURLWithString:@"http://xtecher.com/Wap/Client/getUserInfo"
                                      params:params
                                  completion:^(id json, JSONModelError *err) {
                                      [SVProgressHUD dismiss];
                                      if (err) {
                                          NSLog(@"getUserInfo error: %@", err);
                                          NSString* str = [NSString stringWithFormat:@"Get users error: %@", params[@"uids"]];
                                          [SVProgressHUD showErrorWithStatus: str];
                                          return ;
                                      }
                                      XTUserListModel* response = [[XTUserListModel alloc] initWithDictionary: json error: &err];
                                      
                                      if (response.status != 1) {
                                          return;
                                      }
                                      [[XTUserPreference sharedInstance] updateContacts:response.data];
                                  }];
}

- (void) onFriendsTapped: (id) sender{
    XTContactsViewController *vc = [XTContactsViewController new];
    [vc reloadDataSource];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void) onCreatePostTapped: (id) sender {
    //TODO: create post
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)dealloc
{
    [self unregisterNotifications];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark - UITabBarDelegate

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    if (item.tag == 0) {
        self.title = @"Home";
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationController.navigationBarHidden = YES;
    }else if (item.tag == 1){
        self.title = @"Discover";
        self.navigationItem.rightBarButtonItem = _createPostItem;
        self.navigationController.navigationBarHidden = YES;
    }else if (item.tag == 2){
        self.title = NSLocalizedString(@"chat", @"Chat");
        self.navigationItem.rightBarButtonItem = _friendsItem;
        self.navigationController.navigationBarHidden = NO;
    }else if (item.tag == 3){
        self.title = @"";
        self.navigationItem.rightBarButtonItem = nil;
        self.navigationController.navigationBarHidden = YES;
    }
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 99) {
        if (buttonIndex != [alertView cancelButtonIndex]) {
            [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:YES completion:^(NSDictionary *info, EMError *error) {
                [[ApplyViewController shareController] clear];
                [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
            } onQueue:nil];
        }
    }
    else if (alertView.tag == 100) {
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
    } else if (alertView.tag == 101) {
        [[NSNotificationCenter defaultCenter] postNotificationName:KNOTIFICATION_LOGINCHANGE object:@NO];
    }
}

#pragma mark - private

-(void)registerNotifications
{
    [self unregisterNotifications];
    
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
    [[EaseMob sharedInstance].callManager addDelegate:self delegateQueue:nil];
}

-(void)unregisterNotifications
{
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
    [[EaseMob sharedInstance].callManager removeDelegate:self];
}

- (void)setupSubviews
{
    self.tabBar.backgroundImage = [[UIImage imageNamed:@"tabbarBackground"] stretchableImageWithLeftCapWidth:25 topCapHeight:25];
    
    _chatVC = [[XTChatViewController alloc] init];
    [_chatVC networkChanged:_connectionState];
    
    _indexVC = [[XTFirstViewController alloc] init];
    
    _discoverVC = [XTDiscoverViewController new];

    _meVC = [XTMeViewController new];

    self.viewControllers = @[_indexVC, _discoverVC, _chatVC, _meVC];
    self.selectedViewController = _indexVC;
}

// 统计未读消息数
-(void)setupUnreadMessageCount
{
    NSArray *conversations = [[[EaseMob sharedInstance] chatManager] conversations];
    NSInteger unreadCount = 0;
    for (EMConversation *conversation in conversations) {
        unreadCount += conversation.unreadMessagesCount;
    }
    if (_chatVC) {
        if (unreadCount > 0) {
            _chatVC.tabBarItem.badgeValue = [NSString stringWithFormat:@"%i",(int)unreadCount];
        }else{
            _chatVC.tabBarItem.badgeValue = nil;
        }
    }
    
    UIApplication *application = [UIApplication sharedApplication];
    [application setApplicationIconBadgeNumber:unreadCount];
}

- (void)networkChanged:(EMConnectionState)connectionState
{
    _connectionState = connectionState;
    [_chatVC networkChanged:connectionState];
}

- (void)callOutWithChatter:(NSNotification *)notification
{
    id object = notification.object;
    if ([object isKindOfClass:[NSDictionary class]]) {
        EMError *error = nil;
        NSString *chatter = [object objectForKey:@"chatter"];
        EMCallSessionType type = [[object objectForKey:@"type"] intValue];
        EMCallSession *callSession = nil;
        if (type == eCallSessionTypeAudio) {
            callSession = [[EaseMob sharedInstance].callManager asyncMakeVoiceCall:chatter timeout:50 error:&error];
        }
        else if (type == eCallSessionTypeVideo){
            callSession = [[EaseMob sharedInstance].callManager asyncMakeVideoCall:chatter timeout:50 error:&error];
        }
        
        if (callSession && !error) {
            [[EaseMob sharedInstance].callManager removeDelegate:self];
            
            //            _callController = nil;
            CallViewController *callController = [[CallViewController alloc] initWithSession:callSession isIncoming:NO];
            callController.modalPresentationStyle = UIModalPresentationOverFullScreen;
            //            _callController = callController;
            [self presentViewController:callController animated:NO completion:nil];
        }
        
        if (error) {
            UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"error", @"error") message:error.description delegate:nil cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
}

- (void)callControllerClose:(NSNotification *)notification
{
    //    [_callController dismissViewControllerAnimated:NO completion:nil];
    //    [[EMSDKFull sharedInstance].callManager removeDelegate:_callController];
    //    _callController = nil;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setActive:YES error:nil];
    
    [[EaseMob sharedInstance].callManager addDelegate:self delegateQueue:nil];
}

#pragma mark - IChatManagerDelegate 消息变化

- (void)didUpdateConversationList:(NSArray *)conversations
{
    [self setupUnreadMessageCount];
    [_chatVC refreshDataSource];
    
    NSMutableSet *uids = [NSMutableSet new];
    [conversations enumerateObjectsUsingBlock:^(EMConversation* obj, NSUInteger idx, BOOL *stop) {
        if (! obj.isGroup) {
            XTUserModel* peer = [[XTUserPreference sharedInstance] contactByID:obj.chatter];
            if (! peer) {
                [uids addObject: obj.chatter];
            }
        }
    }];
    [self updateContactsInfo: [uids allObjects]];
}

- (void) didAcceptBuddySucceed:(NSString *)username {
    [self updateContactsInfo: @[username]];

}

- (void) didAcceptedByBuddy:(NSString *)username {
    [self updateContactsInfo: @[username]];
}

// 未读消息数量变化回调
-(void)didUnreadMessagesCountChanged
{
    [self setupUnreadMessageCount];
}

- (void)didFinishedReceiveOfflineMessages:(NSArray *)offlineMessages
{
    [self setupUnreadMessageCount];
}

- (void)didFinishedReceiveOfflineCmdMessages:(NSArray *)offlineCmdMessages
{
    
}

- (BOOL)needShowNotification:(NSString *)fromChatter
{
    BOOL ret = YES;
    NSArray *igGroupIds = [[EaseMob sharedInstance].chatManager ignoredGroupIds];
    for (NSString *str in igGroupIds) {
        if ([str isEqualToString:fromChatter]) {
            ret = NO;
            break;
        }
    }
    
    return ret;
}

// 收到消息回调
-(void)didReceiveMessage:(EMMessage *)message
{
    BOOL needShowNotification = message.isGroup ? [self needShowNotification:message.conversationChatter] : YES;
    if (needShowNotification) {
#if !TARGET_IPHONE_SIMULATOR
        
        BOOL isAppActivity = [[UIApplication sharedApplication] applicationState] == UIApplicationStateActive;
        if (!isAppActivity) {
            [self showNotificationWithMessage:message];
        }else {
            [self playSoundAndVibration];
        }
#endif
    }
}

-(void)didReceiveCmdMessage:(EMMessage *)message
{
    [self showHint:NSLocalizedString(@"receiveCmd", @"receive cmd message")];
}

- (void)playSoundAndVibration{
    NSTimeInterval timeInterval = [[NSDate date]
                                   timeIntervalSinceDate:self.lastPlaySoundDate];
    if (timeInterval < kDefaultPlaySoundInterval) {
        //如果距离上次响铃和震动时间太短, 则跳过响铃
        NSLog(@"skip ringing & vibration %@, %@", [NSDate date], self.lastPlaySoundDate);
        return;
    }
    
    //保存最后一次响铃时间
    self.lastPlaySoundDate = [NSDate date];
    
    // 收到消息时，播放音频
    //[[EaseMob sharedInstance].deviceManager asyncPlayNewMessageSound];
    // 收到消息时，震动
    //[[EaseMob sharedInstance].deviceManager asyncPlayVibration];
}

- (void)showNotificationWithMessage:(EMMessage *)message
{
    EMPushNotificationOptions *options = [[EaseMob sharedInstance].chatManager pushNotificationOptions];
    //发送本地推送
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.fireDate = [NSDate date]; //触发通知的时间
    
    if (options.displayStyle == ePushNotificationDisplayStyle_messageSummary) {
        id<IEMMessageBody> messageBody = [message.messageBodies firstObject];
        NSString *messageStr = nil;
        switch (messageBody.messageBodyType) {
            case eMessageBodyType_Text:
            {
                messageStr = ((EMTextMessageBody *)messageBody).text;
            }
                break;
            case eMessageBodyType_Image:
            {
                messageStr = NSLocalizedString(@"message.image", @"Image");
            }
                break;
            case eMessageBodyType_Location:
            {
                messageStr = NSLocalizedString(@"message.location", @"Location");
            }
                break;
            case eMessageBodyType_Voice:
            {
                messageStr = NSLocalizedString(@"message.voice", @"Voice");
            }
                break;
            case eMessageBodyType_Video:{
                messageStr = NSLocalizedString(@"message.vidio", @"Vidio");
            }
                break;
            default:
                break;
        }
        
        NSString *title = message.from;
        if (message.isGroup) {
            NSArray *groupArray = [[EaseMob sharedInstance].chatManager groupList];
            for (EMGroup *group in groupArray) {
                if ([group.groupId isEqualToString:message.conversationChatter]) {
                    title = [NSString stringWithFormat:@"%@(%@)", message.groupSenderName, group.groupSubject];
                    break;
                }
            }
        }
        
        notification.alertBody = [NSString stringWithFormat:@"%@:%@", title, messageStr];
    }
    else{
        notification.alertBody = NSLocalizedString(@"receiveMessage", @"you have a new message");
    }
    
#warning 去掉注释会显示[本地]开头, 方便在开发中区分是否为本地推送
    //notification.alertBody = [[NSString alloc] initWithFormat:@"[本地]%@", notification.alertBody];
    
    notification.alertAction = NSLocalizedString(@"open", @"Open");
    notification.timeZone = [NSTimeZone defaultTimeZone];
    notification.soundName = UILocalNotificationDefaultSoundName;
    //发送通知
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    //    UIApplication *application = [UIApplication sharedApplication];
    //    application.applicationIconBadgeNumber += 1;
}

#pragma mark - IChatManagerDelegate 登陆回调（主要用于监听自动登录是否成功）

- (void)didLoginWithInfo:(NSDictionary *)loginInfo error:(EMError *)error
{
    if (error) {
        NSString *hintText = NSLocalizedString(@"reconnection.retry", @"Fail to log in your account, is try again... \nclick 'logout' button to jump to the login page \nclick 'continue to wait for' button for reconnection successful");
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt")
                                                            message:hintText
                                                           delegate:self
                                                  cancelButtonTitle:NSLocalizedString(@"reconnection.wait", @"continue to wait")
                                                  otherButtonTitles:NSLocalizedString(@"logout", @"Logout"),
                                  nil];
        alertView.tag = 99;
        [alertView show];
        [_chatVC isConnect:NO];
    }
}

#pragma mark - IChatManagerDelegate 好友变化

- (void)didReceiveBuddyRequest:(NSString *)username
                       message:(NSString *)message
{
#if !TARGET_IPHONE_SIMULATOR
    [self playSoundAndVibration];
    
    BOOL isAppActivity = [[UIApplication sharedApplication] applicationState] == UIApplicationStateActive;
    if (!isAppActivity) {
        //发送本地推送
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = [NSDate date]; //触发通知的时间
        notification.alertBody = [NSString stringWithFormat:NSLocalizedString(@"friend.somebodyAddWithName", @"%@ add you as a friend"), username];
        notification.alertAction = NSLocalizedString(@"open", @"Open");
        notification.timeZone = [NSTimeZone defaultTimeZone];
    }
#endif
    
}

- (void)didUpdateBuddyList:(NSArray *)buddyList
            changedBuddies:(NSArray *)changedBuddies
                     isAdd:(BOOL)isAdd
{
    if (!isAdd)
    {
        NSMutableArray *deletedBuddies = [NSMutableArray array];
        for (EMBuddy *buddy in changedBuddies)
        {
            [deletedBuddies addObject:buddy.username];
        }
        [[EaseMob sharedInstance].chatManager removeConversationsByChatters:deletedBuddies deleteMessages:YES append2Chat:YES];
        [_chatVC refreshDataSource];
    }
}

- (void)didRemovedByBuddy:(NSString *)username
{
    [[EaseMob sharedInstance].chatManager removeConversationByChatter:username deleteMessages:YES append2Chat:YES];
    [_chatVC refreshDataSource];
}

- (void)didRejectedByBuddy:(NSString *)username
{
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"friend.beRefusedToAdd", @"you are shameless refused by '%@'"), username];
    TTAlertNoTitle(message);
}

#pragma mark - IChatManagerDelegate 群组变化

- (void)didReceiveGroupInvitationFrom:(NSString *)groupId
                              inviter:(NSString *)username
                              message:(NSString *)message
{
#if !TARGET_IPHONE_SIMULATOR
    [self playSoundAndVibration];
#endif
    
    // [_contactsVC reloadGroupView];
}

//接收到入群申请
- (void)didReceiveApplyToJoinGroup:(NSString *)groupId
                         groupname:(NSString *)groupname
                     applyUsername:(NSString *)username
                            reason:(NSString *)reason
                             error:(EMError *)error
{
    if (!error) {
#if !TARGET_IPHONE_SIMULATOR
        [self playSoundAndVibration];
#endif
        
        // [_contactsVC reloadGroupView];
    }
}

- (void)didReceiveGroupRejectFrom:(NSString *)groupId
                          invitee:(NSString *)username
                           reason:(NSString *)reason
{
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"friend.beRefusedToAdd", @"you are shameless refused by '%@'"), username];
    TTAlertNoTitle(message);
}


- (void)didReceiveAcceptApplyToJoinGroup:(NSString *)groupId
                               groupname:(NSString *)groupname
{
    NSString *message = [NSString stringWithFormat:NSLocalizedString(@"group.agreedToJoin", @"agreed to join the group of \'%@\'"), groupname];
    [self showHint:message];
}

#pragma mark - IChatManagerDelegate 登录状态变化

- (void)didLoginFromOtherDevice
{
    [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:NO completion:^(NSDictionary *info, EMError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"loginAtOtherDevice", @"your login account has been in other places") delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
        alertView.tag = 100;
        [alertView show];
        
    } onQueue:nil];
}

- (void)didRemovedFromServer
{
    [[EaseMob sharedInstance].chatManager asyncLogoffWithUnbindDeviceToken:NO completion:^(NSDictionary *info, EMError *error) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"prompt", @"Prompt") message:NSLocalizedString(@"loginUserRemoveFromServer", @"your account has been removed from the server side") delegate:self cancelButtonTitle:NSLocalizedString(@"ok", @"OK") otherButtonTitles:nil, nil];
        alertView.tag = 101;
        [alertView show];
    } onQueue:nil];
}

//- (void)didConnectionStateChanged:(EMConnectionState)connectionState
//{
//    [_chatListVC networkChanged:connectionState];
//}

#pragma mark - 自动登录回调

- (void)willAutoReconnect{
    [self hideHud];
    [self showHint:NSLocalizedString(@"reconnection.ongoing", @"reconnecting...")];
}

- (void)didAutoReconnectFinishedWithError:(NSError *)error{
    [self hideHud];
    if (error) {
        [self showHint:NSLocalizedString(@"reconnection.fail", @"reconnection failure, later will continue to reconnection")];
    }else{
        NSLog(NSLocalizedString(@"reconnection.success", @"reconnection successful！"));
    }
}

#pragma mark - ICallManagerDelegate

- (void)callSessionStatusChanged:(EMCallSession *)callSession changeReason:(EMCallStatusChangedReason)reason error:(EMError *)error
{
    if (callSession.status == eCallSessionStatusConnected)
    {
        EMError *error = nil;
        BOOL isShowPicker = [[[NSUserDefaults standardUserDefaults] objectForKey:@"isShowPicker"] boolValue];
        
#warning 在后台不能进行视频通话
        if(callSession.type == eCallSessionTypeVideo && [[UIApplication sharedApplication] applicationState] == UIApplicationStateBackground){
            error = [EMError errorWithCode:EMErrorInitFailure andDescription:@"后台不能进行视频通话"];
        }
        else if (!isShowPicker){
            [[EaseMob sharedInstance].callManager removeDelegate:self];
            //            _callController = nil;
            CallViewController *callController = [[CallViewController alloc] initWithSession:callSession isIncoming:YES];
            callController.modalPresentationStyle = UIModalPresentationOverFullScreen;
            //            _callController = callController;
            [self presentViewController:callController animated:NO completion:nil];
        }
        
        if (error || isShowPicker) {
            [[EaseMob sharedInstance].callManager asyncEndCall:callSession.sessionId reason:eCallReason_Hangup];
        }
    }
}

#pragma mark - public

- (void)jumpToChatList
{
    if(_chatVC)
    {
        [self.navigationController popToViewController:self animated:NO];
        [self setSelectedViewController:_chatVC];
    }
}

@end
