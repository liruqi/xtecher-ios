//
//  LoginViewController.m
//  xtecher
//
//  Created by liruqi on 5/2/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "LoginViewController.h"
#import "XTUserPreference.h"
#import "XTTabBarController.h"
#import "JSONModel.h"
#import "JSONHTTPClient.h"

@interface LoginViewController () <UIWebViewDelegate>

@property (retain, nonatomic) UIWebView *webView;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // check if logged in
    self.view.backgroundColor = [UIColor clearColor];
    CGRect rect = [[UIScreen mainScreen] bounds];
    self.webView = [[UIWebView alloc] initWithFrame: rect];
    self.webView.scrollView.bounces = NO;
    [self.view addSubview:self.webView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //TODO: check if has login
    

    [self showLogin];
    
    
//    //make post, get requests
//    [JSONHTTPClient postJSONFromURLWithString:@"http://mydomain.com/api"
//                                       params:@{@"postParam1":@"value1"}
//                                   completion:^(id json, JSONModelError *err) {
//                                       
//                                       //check err, process json ...
//                                       
//                                   }];

}

- (void)showLogin {
    self.webView.delegate = self;
    self.webView.hidden = NO;
    NSURL *loginURL = [NSURL URLWithString: [NSString stringWithFormat:@"http://xtecher.com%@?client=ios", XT_LOGIN_PATH]];
    [self.webView loadRequest:[NSURLRequest requestWithURL:loginURL]];
}

//- (void)showHomeView {
//    self.webView.hidden = YES;
//    
//    [self presentViewController:[[UINavigationController alloc] initWithRootViewController:[XTTabBarController new]]
//                       animated:YES
//                     completion:^{
//        
//    }];
//}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = request.URL;
    if ([url.scheme isEqual:@"xtecher"]) {
        XTUser *user = [XTUser fromURL:url];
        if (user) {
            // save user
            [XTUserPreference setCurrentUser:user];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self  dismissViewControllerAnimated:YES completion:^{
                    
                }];
            });
        } else if ([url.host isEqualToString:@"openurl"]) {
            NSURL* ourl = [NSURL URLWithString:[@"http:/" stringByAppendingString:url.path]];
            if (ourl) {
                [[UIApplication sharedApplication] openURL:ourl];
            }
        }
        return NO;
    }
    return YES;
}

@end
