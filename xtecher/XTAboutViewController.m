//
//  XTAboutViewController.m
//  xtecher
//
//  Created by liruqi on 6/20/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTAboutViewController.h"

@interface XTAboutViewController ()
@property (weak, nonatomic) IBOutlet UIView *footView;

@end

@implementation XTAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.footView.backgroundColor = RGBACOLOR(0x16, 0x1d, 0x25, 1);
    self.navigationItem.title = NSLocalizedString(@"About Xtecher", @"About Xtecher");
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
