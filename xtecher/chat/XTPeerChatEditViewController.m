//
//  XTGroupEditTableViewController.m
//  xtecher
//
//  Created by liruqi on 6/7/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTPeerChatEditViewController.h"

#import "ContactView.h"
#import "GroupSettingViewController.h"
#import "GroupSubjectChangingViewController.h"
#import "EMChooseViewController.h"
#import "SVProgressHUD.h"
#import "JSONHTTPClient.h"
#import "XTUserPreference.h"
#import "XTContactSelectViewController.h"
#import "XTGroupChatViewController.h"

#define kColOfRow 4
#define kContactSize 60

@interface XTPeerChatEditViewController () <EMChooseViewDelegate>
{
    BOOL _inBlackList;
    NSArray *_friends;
    XTUserModel* _me;
}
@property (strong, nonatomic) XTUserModel *chatter;

@property (strong, nonatomic) NSMutableArray *dataSource;
@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) UIButton *addButton;

@property (strong, nonatomic) UIButton *configureButton;
@property (strong, nonatomic) UILongPressGestureRecognizer *longPress;
@property (strong, nonatomic) ContactView *selectedContact;

@end

@implementation XTPeerChatEditViewController

- (void)registerNotifications {
    [self unregisterNotifications];
    [[EaseMob sharedInstance].chatManager addDelegate:self delegateQueue:nil];
}

- (void)unregisterNotifications {
    [[EaseMob sharedInstance].chatManager removeDelegate:self];
}

- (void)dealloc {
    [self unregisterNotifications];
}

- (instancetype)initWithUser:(XTUserModel *)user
{
    self = [super init];
    if (self) {
        XTUser* xtuser = [XTUserPreference currentUser];
        _me = [[XTUserPreference sharedInstance] contactByID:xtuser.uid];
        self.chatter = user;
        self.dataSource = [[NSMutableArray alloc] initWithObjects: user, nil];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView:)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    [self refreshScrollView];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];

    [SVProgressHUD show];
    [[XTUserPreference sharedInstance] requestFriendsListWithBlock:^(NSArray* friends, NSError* e) {
        [SVProgressHUD dismiss];
        if (e) {
            [SVProgressHUD showErrorWithStatus:@"获取好友列表失败"];
            [self.navigationController popToRootViewControllerAnimated:YES];
            return ;
        }
        _friends = [friends copy];
    }];
    
    _inBlackList = NO;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        EMError* pError = NULL;
        NSArray* uids = [[EaseMob sharedInstance].chatManager fetchBlockedList:& pError];
        
        if (pError) {
            return ;
        }
        
        for (NSString* uid in uids) {
            if ([uid isEqualToString:self.chatter.uid]) {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    _inBlackList = YES;
                    [self.tableView reloadData];
                });
                break;
            }
        }
    });
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

#pragma mark - getter

- (UIScrollView *)scrollView
{
    if (_scrollView == nil) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(10, 10, self.view.frame.size.width - 20, kContactSize)];
        _scrollView.tag = 0;
        
        _addButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kContactSize - 10, kContactSize - 10)];
        [_addButton setImage:[UIImage imageNamed:@"group_participant_add"] forState:UIControlStateNormal];
        [_addButton setImage:[UIImage imageNamed:@"group_participant_addHL"] forState:UIControlStateHighlighted];
        [_addButton addTarget:self action:@selector(addContact:) forControlEvents:UIControlEventTouchUpInside];
        
        _longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:_scrollView action:@selector(deleteContactBegin:)];
        _longPress.minimumPressDuration = 0.5;
    }
    
    return _scrollView;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    // Configure the cell...
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:CellIdentifier];
    }
    
    if (indexPath.row == 0) {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell.contentView addSubview:self.scrollView];
    }
    
    //    cell.textLabel.text = NSLocalizedString(@"group.occupantCount", @"members count");
    //    cell.accessoryType = UITableViewCellAccessoryNone;
    // cell.detailTextLabel.text = [NSString stringWithFormat:@"%i / %i", (int)[_chatGroup.occupants count], (int)_chatGroup.groupSetting.groupMaxUsersCount];
    
    else if (indexPath.row == 1)
    {
        cell.textLabel.text = NSLocalizedString(@"title.block", @"Block");
        UISwitch* blockSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 60, 44)];
        [blockSwitch setOn:_inBlackList];
        [blockSwitch addTarget:self action:@selector(onBlockStateChanged:) forControlEvents:UIControlEventValueChanged];
        cell.accessoryView = blockSwitch;
    } else if (indexPath.row == 2)
    {
        cell.textLabel.text = NSLocalizedString(@"title.report", @"Report");
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    
    return cell;
}

- (void) onBlockStateChanged: (id) sender {
    BOOL state = [sender isOn];
    if (state != _inBlackList) {
        _inBlackList = state;
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
            if (state) {
                EMError *error = [[EaseMob sharedInstance].chatManager blockBuddy:self.chatter.uid relationship:eRelationshipBoth];
            } else {
                EMError *error = [[EaseMob sharedInstance].chatManager
                                  unblockBuddy:self.chatter.uid
                                  ];
            }
        });
    }
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row = (int)indexPath.row;
    if (row == 0) {
        return self.scrollView.frame.size.height + 40;
    }
    else {
        return 50;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 1)
    {
        
    } else if (indexPath.row == 2) {
        [SVProgressHUD show];
        [JSONHTTPClient getJSONFromURLWithString:@"http://xtecher.com/Common/Report/reportUser"
                                          params:@{@"uid": self.chatter.uid}
                                      completion:^(id json, JSONModelError *err) {
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              [SVProgressHUD dismiss];
                                              [SVProgressHUD showSuccessWithStatus:@"Done"];
                                          });
                                          
                                      }];
    }
}

#pragma mark - EMChooseViewDelegate
- (void)viewController:(EMChooseViewController *)viewController didFinishSelectedSources:(NSArray *)selectedSources
{
    
    for (XTUserModel *user in selectedSources) {
        [self.dataSource addObject:user];
    }
    
    if (self.dataSource.count <= 1) {
        return;
    }
    
    [self refreshScrollView];

    NSMutableArray* uids = [NSMutableArray array];
    [self.dataSource enumerateObjectsUsingBlock:^(XTUserModel* user, NSUInteger idx, BOOL* stop) {
        [uids addObject:user.uid];
    }];
    NSString *groupName = [NSString stringWithFormat:@"%@'s Group", _me.name];
    NSString *messageStr = [NSString stringWithFormat:@"%@ invite you to join group", _me.name];
    EMGroupStyleSetting *setting = [[EMGroupStyleSetting alloc] init];
    setting.groupStyle = eGroupStyle_PrivateMemberCanInvite;
    setting.groupMaxUsersCount = KSDK_GROUP_MAXUSERSCOUNT;

    [SVProgressHUD show];
    [[EaseMob sharedInstance].chatManager asyncCreateGroupWithSubject:groupName
                                                          description:@""
                                                             invitees:uids
                                                initialWelcomeMessage:messageStr
                                                         styleSetting:setting
                                                           completion:^(EMGroup *group, EMError *error) {
        [SVProgressHUD dismiss];
        if (group && !error) {
            [SVProgressHUD showSuccessWithStatus:NSLocalizedString(@"group.create.success", @"create group success")];
            NSMutableArray* vcs = [[self.navigationController viewControllers] mutableCopy];
            XTGroupChatViewController *groupChatVC = [[XTGroupChatViewController alloc] initWithGID:group.groupId];
            [self.navigationController setViewControllers:@[vcs.firstObject, groupChatVC] animated:YES];
        } else {
            [SVProgressHUD showErrorWithStatus:NSLocalizedString(@"group.create.fail", @"Failed to create a group, please operate again")];
        }
    } onQueue:nil];
}

- (NSArray*)viewControllerLoadDataSource:(EMChooseViewController *)viewController {
    return _friends;
}

- (void)refreshScrollView
{
    [self.scrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.scrollView removeGestureRecognizer:_longPress];
    [self.addButton removeFromSuperview];
    
    BOOL showAddButton = YES;
    [self.scrollView addSubview:self.addButton];
    
    int tmp = ([self.dataSource count] + 1) % kColOfRow;
    int row = (int)([self.dataSource count] + 1) / kColOfRow;
    row += tmp == 0 ? 0 : 1;
    self.scrollView.tag = row;
    self.scrollView.frame = CGRectMake(10, 20, self.tableView.frame.size.width - 20, row * kContactSize);
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width, row * kContactSize);
    
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
    
    int i = 0;
    int j = 0;
    BOOL isEditing = self.addButton.hidden ? YES : NO;
    BOOL isEnd = NO;
    for (i = 0; i < row; i++) {
        for (j = 0; j < kColOfRow; j++) {
            NSInteger index = i * kColOfRow + j;
            if (index < [self.dataSource count]) {
                XTUserModel *userModel = [self.dataSource objectAtIndex:index];
                ContactView *contactView = [[ContactView alloc] initWithFrame:CGRectMake(j * kContactSize, i * kContactSize, kContactSize, kContactSize)];
                contactView.index = i * kColOfRow + j;
                contactView.image = [UIImage imageNamed:@"chatListCellHead.png"];
                [contactView setImageURL:[NSURL URLWithString:userModel.avator]];
                contactView.remark = userModel.name;
                if (![userModel.uid isEqualToString:loginUsername]) {
                    contactView.editing = isEditing;
                }
                
                //[self.dataSource removeObjectAtIndex:index];
                
                [self.scrollView addSubview:contactView];
            }
            else{
                if(showAddButton && index == self.dataSource.count)
                {
                    self.addButton.frame = CGRectMake(j * kContactSize + 5, i * kContactSize + 10, kContactSize - 10, kContactSize - 10);
                }
                
                isEnd = YES;
                break;
            }
        }
        
        if (isEnd) {
            break;
        }
    }
    
    [self.tableView reloadData];
}

#pragma mark - action

- (void)tapView:(UITapGestureRecognizer *)tap
{
    if (tap.state == UIGestureRecognizerStateEnded)
    {
        if (self.addButton.hidden) {
            [self setScrollViewEditing:NO];
        }
    }
}

- (void)deleteContactBegin:(UILongPressGestureRecognizer *)longPress
{
    if (longPress.state == UIGestureRecognizerStateBegan)
    {
        NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
        NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
        for (ContactView *contactView in self.scrollView.subviews)
        {
            CGPoint locaton = [longPress locationInView:contactView];
            if (CGRectContainsPoint(contactView.bounds, locaton))
            {
                if ([contactView isKindOfClass:[ContactView class]]) {
                    if ([contactView.remark isEqualToString:loginUsername]) {
                        return;
                    }
                    _selectedContact = contactView;
                    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:NSLocalizedString(@"cancel", @"cancel") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"delete", @"deleting member..."), nil];
                    [sheet showInView:self.view];
                }
            }
        }
    }
}

- (void)setScrollViewEditing:(BOOL)isEditing
{
    NSDictionary *loginInfo = [[[EaseMob sharedInstance] chatManager] loginInfo];
    NSString *loginUsername = [loginInfo objectForKey:kSDKUsername];
    
    for (ContactView *contactView in self.scrollView.subviews)
    {
        if ([contactView isKindOfClass:[ContactView class]]) {
            if ([contactView.remark isEqualToString:loginUsername]) {
                continue;
            }
            
            [contactView setEditing:isEditing];
        }
    }
    
    self.addButton.hidden = isEditing;
}

- (void)addContact:(id)sender
{
    NSMutableArray* uids = [NSMutableArray array];
    [self.dataSource enumerateObjectsUsingBlock:^(XTUserModel* user, NSUInteger idx, BOOL *stop) {
        [uids addObject:user.uid];
    }];
    XTContactSelectViewController *selectionController = [[XTContactSelectViewController alloc] initWithBlockSelectedUsernames: uids];
    selectionController.delegate = self;
    [self.navigationController pushViewController:selectionController animated:YES];
}

- (void)didIgnoreGroupPushNotification:(NSArray *)ignoredGroupList error:(EMError *)error {
    // todo
    NSLog(@"ignored group list:%@.", ignoredGroupList);
}

#pragma mark - UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSInteger index = _selectedContact.index;
    if (buttonIndex == 0)
    {
        //delete
        _selectedContact.deleteContact(index);
    }
    _selectedContact = nil;
}

- (void)actionSheetCancel:(UIActionSheet *)actionSheet
{
    _selectedContact = nil;
}


@end
