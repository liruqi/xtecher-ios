//
//  XTChangeGroupNameViewController.h
//  xtecher
//
//  Created by RUQI LI on 11/6/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EMGroup.h"

@interface XTChangeGroupNameViewController : UIViewController

- (instancetype)initWithGroup:(EMGroup *)group;

@end
