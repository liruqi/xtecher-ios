//
//  XTContactSelectViewController.h
//  xtecher
//
//  Created by liruqi on 6/9/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "EMChooseViewController.h"

@interface XTContactSelectViewController : EMChooseViewController

- (instancetype)initWithBlockSelectedUsernames:(NSArray *)usernames;

@end
