//
//  XTGroupChatViewController.h
//  xtecher
//
//  Created by liruqi on 6/6/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "ChatViewController.h"

@interface XTGroupChatViewController : ChatViewController

- (instancetype)initWithGID:(NSString *)uid;

@end
