//
//  XTChatRoomViewController.h
//  xtecher
//
//  Created by liruqi on 5/19/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "ChatViewController.h"
#import "XTUserModel.h"

@interface XTPeerChatViewController : ChatViewController

- (instancetype)initWithChatter:(XTUserModel *)user;
- (instancetype)initWithUID:(NSString *)uid;

@end
