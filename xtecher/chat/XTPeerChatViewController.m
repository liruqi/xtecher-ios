//
//  XTChatRoomViewController.m
//  xtecher
//
//  Created by liruqi on 5/19/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTPeerChatViewController.h"
#import "SVProgressHUD.h"
#import "XTUserPreference.h"
#import "JSONHTTPClient.h"
#import "XTUserListModel.h"
#import "NSDate+Category.h"
#import "MessageModelManager.h"
#import "MessageModel.h"
#import "EMChatBaseBubbleView.h"
#import "EMChatViewBaseCell.h"
#import "XTWebPageViewController.h"
#import "ChatGroupDetailViewController.h"
#import "XTPeerChatEditViewController.h"

@interface XTPeerChatViewController ()
{
    XTUserModel *_peer;
    XTUserModel *_me;
    NSString *_peerUID;
    BOOL _previousNavigationBarHidden;
}

@end

@implementation XTPeerChatViewController

- (instancetype)initWithChatter:(XTUserModel *)user {
    self = [super initWithChatter:user.uid isGroup:NO];
    if (self) {
        _peer = user;
        XTUser *user = [XTUserPreference currentUser];
        _me = [[XTUserPreference sharedInstance] contactByID:user.uid];
    }
    
    return self;
}

- (instancetype)initWithUID:(NSString *)uid {
    self = [super initWithChatter:uid isGroup:NO];
    _peerUID = uid;
    XTUser *user = [XTUserPreference currentUser];
    _me = [[XTUserPreference sharedInstance] contactByID:user.uid];
    _peer = [[XTUserPreference sharedInstance] contactByID:uid];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _previousNavigationBarHidden = self.navigationController.navigationBarHidden;
    [self setupBarButtonItem];
    if (_peer) {
        return;
    }
    if (! _peerUID) {
        return;
    }
    
//    if (_peer) {
//        [self updatePeerInfo];
//        [self reloadData];
//        return;
//    }
//    [SVProgressHUD show];
    
    [[XTUserPreference sharedInstance] requestContacts:@[_peerUID] withBlock:^(JSONModelError* e) {
//        [SVProgressHUD dismiss];
        if (e) {
            return;
        }
        _peer = [[XTUserPreference sharedInstance] contactByID:_peerUID];
        [self updatePeerInfo];
        [self reloadData];
    }];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updatePeerInfo];
    self.navigationController.navigationBarHidden = NO;
}

- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = _previousNavigationBarHidden;
}

- (void)setupBarButtonItem
{
    UIImage* detailImg = [UIImage imageNamed:@"peer_detail.png"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:detailImg style:UIBarButtonItemStylePlain target:self action:@selector(showRoomContact:)];
}

- (void)showRoomContact:(id)sender
{
    [self.view endEditing:YES];
    XTUserModel* chatterModel = [[XTUserPreference sharedInstance] contactByID:self.chatter];
    XTPeerChatEditViewController *detailController = [[XTPeerChatEditViewController alloc] initWithUser: chatterModel];
    [self.navigationController pushViewController:detailController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) updatePeerInfo {
    if (_peer) {
        self.title = _peer.name;
    }
}

#pragma mark - Override
- (NSArray *)formatMessages:(NSArray *)messagesArray
{
    NSMutableArray *formatArray = [[NSMutableArray alloc] init];

    if ([messagesArray count] > 0) {
        for (EMMessage *message in messagesArray) {
            NSDate *createDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:(NSTimeInterval)message.timestamp];
            NSTimeInterval tempDate = [createDate timeIntervalSinceDate:self.chatTagDate];
            if (tempDate > 60 || tempDate < -60 || (self.chatTagDate == nil)) {
                [formatArray addObject:[createDate formattedTime]];
                self.chatTagDate = createDate;
            }
            
            MessageModel *model = [MessageModelManager modelWithMessage:message];
            if (model.isSender) {
                model.headImageURL = [NSURL URLWithString:_me.avator];
            } else {
                model.headImageURL = [NSURL URLWithString:_peer.avator];
            }
            if (model) {
                [formatArray addObject:model];
            }
        }
    }
    
    return formatArray;
}


-(NSMutableArray *)formatMessage:(EMMessage *)message
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    NSDate *createDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:(NSTimeInterval)message.timestamp];
    NSTimeInterval tempDate = [createDate timeIntervalSinceDate:self.chatTagDate];
    if (tempDate > 60 || tempDate < -60 || (self.chatTagDate == nil)) {
        [ret addObject:[createDate formattedTime]];
        self.chatTagDate = createDate;
    }
    
    MessageModel *model = [MessageModelManager modelWithMessage:message];
    if (model.isSender) {
        model.headImageURL = [NSURL URLWithString:_me.avator];
    } else {
        model.headImageURL = [NSURL URLWithString:_peer.avator];
    }
    if (model) {
        [ret addObject:model];
    }
    
    return ret;
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    MessageModel *model = [userInfo objectForKey:KMESSAGEKEY];
    if ([eventName isEqualToString:kRouterEventChatHeadImageTapEventName]) {
        XTUser *user = [XTUserPreference currentUser];
        NSString* strURL = [NSString stringWithFormat:XT_PROFILE_URL, model.username, user.token];
        NSURL* uri = [NSURL URLWithString:strURL];
        if (uri) {
            XTWebPageViewController *pageVC = [[XTWebPageViewController alloc] initWithURL: uri];
            pageVC.barHidden = YES;
            [self.navigationController pushViewController:pageVC animated:YES];
        }
        return;
    }
    
    [super routerEventWithName:eventName userInfo:userInfo];
}
@end
