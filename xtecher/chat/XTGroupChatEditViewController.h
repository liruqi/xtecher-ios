//
//  XTChatEditViewController.h
//  xtecher
//
//  Created by liruqi on 6/6/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "ChatGroupDetailViewController.h"

@interface XTGroupChatEditViewController : UITableViewController<IChatManagerDelegate>

- (instancetype)initWithGroup:(EMGroup *)chatGroup;

@end
