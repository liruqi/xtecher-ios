//
//  XTGroupChatViewController.m
//  xtecher
//
//  Created by liruqi on 6/6/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTGroupChatViewController.h"
#import "NSDate+Category.h"
#import "MessageModel.h"
#import "XTUserModel.h"
#import "XTUserPreference.h"
#import "MessageModelManager.h"
#import "JSONHTTPClient.h"
#import "SVProgressHUD.h"
#import "XTWebPageViewController.h"
#import "EMChatViewBaseCell.h"
#import "XTGroupChatEditViewController.h"

@interface XTGroupChatViewController ()
{
    XTUserModel *_me;
    EMGroup *_chatGroup;
}
@end

@implementation XTGroupChatViewController

- (instancetype)initWithGID:(NSString *)uid {
    self = [super initWithChatter:uid isGroup:YES];
    if (self) {
        XTUser *user = [XTUserPreference currentUser];
        _me = [[XTUserPreference sharedInstance] contactByID:user.uid];
        
        NSArray *groupArray = [[EaseMob sharedInstance].chatManager groupList];
        for (EMGroup *group in groupArray) {
            if ([group.groupId isEqualToString:uid]) {
                _chatGroup = group;
                break;
            }
        }
        
        if (_chatGroup == nil) {
            _chatGroup = [EMGroup groupWithId:uid];
        }
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage* detailImg = [UIImage imageNamed:@"group_detail.png"];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithImage:detailImg style:UIBarButtonItemStylePlain target:self action:@selector(showRoomContact:)];
    self.navigationItem.title = _chatGroup.groupSubject;

//    NSMutableArray *uids = [NSMutableArray array];
//    for (NSString *uid in _chatGroup.members) {
//        XTUserModel *member = [[XTUserPreference sharedInstance] contactByID:uid];
//        if (member) {
//            continue;
//        }
//        [uids addObject:uid];
//    }
//    if (uids.count == 0) {
//        return;
//    }
    
    NSArray *uids = [_chatGroup.members copy];

//    [SVProgressHUD show];
    [[XTUserPreference sharedInstance] requestContacts:uids withBlock:^(JSONModelError* e) {
//        [SVProgressHUD dismiss];
        if (e) {
            return;
        }
        [self reloadData];
    }];
    
}

- (void)showRoomContact:(id)sender
{
    [self.view endEditing:YES];
        
    XTGroupChatEditViewController *detailController = [[XTGroupChatEditViewController alloc] initWithGroup:_chatGroup];
    [self.navigationController pushViewController:detailController animated:YES];
}

#pragma mark - Override
- (NSArray *)formatMessages:(NSArray *)messagesArray
{
    NSMutableArray *formatArray = [[NSMutableArray alloc] init];
    
    if ([messagesArray count] > 0) {
        NSMutableArray *uids = [NSMutableArray array];

        for (EMMessage *message in messagesArray) {
            NSDate *createDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:(NSTimeInterval)message.timestamp];
            NSTimeInterval tempDate = [createDate timeIntervalSinceDate:self.chatTagDate];
            if (tempDate > 60 || tempDate < -60 || (self.chatTagDate == nil)) {
                [formatArray addObject:[createDate formattedTime]];
                self.chatTagDate = createDate;
            }
            
            MessageModel *model = [MessageModelManager modelWithMessage:message];
            if (model.isSender) {
                model.headImageURL = [NSURL URLWithString:_me.avator];
            } else {
                XTUserModel* member = [[XTUserPreference sharedInstance] contactByID:model.username];
                if (member) {
                    model.headImageURL = [NSURL URLWithString: member.avator];
                } else {
                    [uids addObject:model.username];
                }
            }
            if (model) {
                [formatArray addObject:model];
            }
        }
        if (uids.count > 0) {
            [[XTUserPreference sharedInstance] requestContacts:uids withBlock:^(JSONModelError* e) {
                if (e) {
                    return;
                }
                [self reloadData];
            }];
        }
    }
    
    return formatArray;
}


-(NSMutableArray *)formatMessage:(EMMessage *)message
{
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    NSDate *createDate = [NSDate dateWithTimeIntervalInMilliSecondSince1970:(NSTimeInterval)message.timestamp];
    NSTimeInterval tempDate = [createDate timeIntervalSinceDate:self.chatTagDate];
    if (tempDate > 60 || tempDate < -60 || (self.chatTagDate == nil)) {
        [ret addObject:[createDate formattedTime]];
        self.chatTagDate = createDate;
    }
    
    MessageModel *model = [MessageModelManager modelWithMessage:message];
    if (model.isSender) {
        model.headImageURL = [NSURL URLWithString:_me.avator];
    } else {
        XTUserModel* member = [[XTUserPreference sharedInstance] contactByID:model.username];
        if (member) {
            model.headImageURL = [NSURL URLWithString: member.avator];
        } else {
            [[XTUserPreference sharedInstance] requestContacts:@[model.username] withBlock:^(JSONModelError* e) {
                if (e) {
                    return;
                }
                [self reloadData];
            }];
        }
    }
    if (model) {
        [ret addObject:model];
    }
    
    return ret;
}

- (void)routerEventWithName:(NSString *)eventName userInfo:(NSDictionary *)userInfo {
    MessageModel *model = [userInfo objectForKey:KMESSAGEKEY];
    if ([eventName isEqualToString:kRouterEventChatHeadImageTapEventName]) {
        XTUser *user = [XTUserPreference currentUser];
        NSString* strURL = [NSString stringWithFormat:XT_PROFILE_URL, model.username, user.token];
        NSURL* uri = [NSURL URLWithString:strURL];
        if (uri) {
            XTWebPageViewController *pageVC = [[XTWebPageViewController alloc] initWithURL: uri];
            pageVC.barHidden = YES;
            [self.navigationController pushViewController:pageVC animated:YES];
        }
        return;
    }
    
    [super routerEventWithName:eventName userInfo:userInfo];
}

- (void)groupDidUpdateInfo:(EMGroup *)group error:(EMError *)error
{
    [super groupDidUpdateInfo:group error:error];
    NSMutableArray *uids = [NSMutableArray array];
    for (NSString *uid in _chatGroup.members) {
        XTUserModel *member = [[XTUserPreference sharedInstance] contactByID:uid];
        if (member) {
            continue;
        }
        [uids addObject:uid];
    }
    if (uids.count == 0) {
        return;
    }
    [[XTUserPreference sharedInstance] requestContacts:uids withBlock:^(JSONModelError* e) {
        if (e) {
            return;
        }
        [self reloadData];
    }];
}

@end
