//
//  XTGroupEditTableViewController.h
//  xtecher
//
//  Created by liruqi on 6/7/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XTUserModel.h"

@interface XTPeerChatEditViewController : UITableViewController<IChatManagerDelegate>

- (instancetype)initWithUser:(XTUserModel *)user;

@end
