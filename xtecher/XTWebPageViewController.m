//
//  XTCreatePostViewController.m
//  xtecher
//
//  Created by liruqi on 5/30/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTWebPageViewController.h"
#import "WXApiObject.h"
#import "WXApi.h"
#import "EMSDWebImageDownloaderOperation.h"

@interface XTWebPageViewController ()
{
    BOOL _navigationBarHidden;
    NSURL *_url;
}

@end

@implementation XTWebPageViewController

-(instancetype) initWithURL: (NSURL*) url {
    self = [super init];
    if(self) {
        NSString* str = [[url absoluteString] stringByAppendingString:@"&back=native"];
        _url = [NSURL URLWithString:str];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.webView loadRequest:[NSURLRequest requestWithURL:_url]];
}


- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _navigationBarHidden = self.navigationController.navigationBarHidden;
    self.navigationController.navigationBarHidden = self.barHidden;
}

- (void) viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = _navigationBarHidden;
}

- (BOOL) shouldStartLoad: (NSURL*) url {
    NSLog(@"shouldStartLoad: %@", url);
    if ([url.path isEqualToString:XT_DISCOVER_PATH]) {
        [super goBack];
    }
    return YES;
}

- (void) xtecherCommand: (NSURL*) url {
    if ([url.host isEqualToString:XT_CMD_CHATROOM]) {
        [super goBack];
        return;
    }
    if ([url.host isEqualToString:XT_CMD_BACK]) {
        [super goBack];
        return;
    }
    if ([url.host isEqualToString:@"openurl"]) {
        NSURL* url = [NSURL URLWithString:[@"http:/" stringByAppendingString:url.path]];
        if (url) {
            [[UIApplication sharedApplication] openURL:url];
        }
        return;
    }
    if ([url.host isEqualToString:XT_CMD_WECHATSHARE]) {
        WXMediaMessage *message = [WXMediaMessage message];
        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];

        if ([url respondsToSelector:@selector(queryItems)]) {
            NSArray *items = [url performSelector:@selector(queryItems)];
            for (NSURLQueryItem *item in items) {
                params[item.name] = [item.value stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
            }
        } else {
            for (NSString *param in [url.query componentsSeparatedByString:@"&"]) {
                NSArray *elts = [param componentsSeparatedByString:@"="];
                if([elts count] < 2) continue;
                [params setObject:[elts[1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding]
                           forKey:elts [0]];
            }
        }
        if (! params[@"title"]) {
            return;
        }
        if (! params[@"description"]) {
            return;
        }
        if (! params[@"url"]) {
            return;
        }
        message.title = params[@"title"];
        message.description = params[@"description"];
        
        [message setThumbImage:[UIImage imageNamed:@"AppIcon"]];
        
        WXWebpageObject *ext = [WXWebpageObject object];
        ext.webpageUrl = params[@"url"];
        
        message.mediaObject = ext;
        message.mediaTagName = @"WECHAT_TAG_JUMP_SHOWRANK";
        NSURL *imageURL = [NSURL URLWithString:params[@"image"]];
        int scene =[url.path isEqualToString:@"/contacts"] ? WXSceneSession : WXSceneTimeline;
        if (imageURL) {
            EMSDWebImageDownloaderOperation *op = [[EMSDWebImageDownloaderOperation alloc]
                                                   initWithRequest:[NSURLRequest requestWithURL:imageURL]
                                                   options:EMSDWebImageDownloaderUseNSURLCache
                                                   progress:^(NSInteger receivedSize, NSInteger expectedSize){}
                                                   completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished){
                                                       message.thumbData = data;
                                                       [self shareWeChatWithMessage:message Scene:scene];
                                                   }cancelled:^{
                                                       [self shareWeChatWithMessage:message Scene:scene];
                                                   }];
            
            NSOperationQueue *que = [[NSOperationQueue alloc] init];
            [que addOperation:op];
        } else {
            [self shareWeChatWithMessage:message Scene:scene];
        }
        return;
    }
    [super xtecherCommand: url];
}

- (void) shareWeChatWithMessage: (WXMediaMessage*) message Scene:(int)scene
{
    SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
    req.bText = NO;
    req.message = message;
    req.scene = scene;
    [WXApi sendReq:req];
}


@end
