//
//  main.m
//  xtecher
//
//  Created by liruqi on 5/2/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
