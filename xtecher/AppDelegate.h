//
//  AppDelegate.h
//  xtecher
//
//  Created by liruqi on 5/2/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "XTTabBarController.h"
#import "WXApi.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, IChatManagerDelegate, WXApiDelegate>
{
    EMConnectionState _connectionState;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) XTTabBarController *mainController;


@end

