//
//  XTWebViewController.m
//  xtecher
//
//  Created by liruqi on 5/19/15.
//  Copyright (c) 2015 xtecher. All rights reserved.
//

#import "XTWebViewController.h"
#import "LoginViewController.h"
#import "XTPeerChatViewController.h"
#import "XTSettingViewController.h"
#import "SettingsViewController.h"
#import "AppDelegate.h"
#import "XTWebPageViewController.h"

const NSString* XT_LOGIN_URL = @"/Wap/UserAccount/loginPage";

@interface XTWebViewController () <XTWebViewDelegate>
{
    CGFloat _tabBarHeight;
}
@end

@implementation XTWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CGRect rect = [[UIScreen mainScreen] bounds];
    CGFloat statusBarHeight = 0; // [UIApplication sharedApplication].statusBarFrame.size.height;
    self.webView = [[XTWebView alloc] initWithFrame: CGRectMake(rect.origin.x, rect.origin.y + statusBarHeight, rect.size.width, rect.size.height-statusBarHeight)];
    self.webView.xtDelegate = self;
    self.webView.scrollView.bounces = NO;
    if (self.tabBarController) {
        _tabBarHeight = CGRectGetHeight(self.tabBarController.tabBar.frame);
        self.webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, _tabBarHeight, 0);
        self.webView.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, _tabBarHeight, 0);
    }
    
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - XTWebViewDelegate
- (BOOL) canGoBack{
    return true;
}

- (void) goBack {
    if (self.navigationController) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
        if (appDelegate.window.rootViewController == self) return;
        [self dismissViewControllerAnimated:YES completion:^{}];
    }
}

#pragma mark - UIWebViewDelegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSURL *url = request.URL;
    NSLog(@"shouldStartLoadWithRequest: %@", url.absoluteString);

    if ([url.scheme isEqual:@"http"]) {
        if ([url.path isEqualToString:XT_LOGIN_PATH]) {
            LoginViewController* vc = [LoginViewController new];
            [self presentViewController:vc animated:YES completion:^(){
            }];
            return NO;
        }
        return [self shouldStartLoad: url];
    }
    if ([url.scheme isEqual:@"xtecher"]) {
        if ([url.host isEqual:@"chat"]) {
            NSCharacterSet* trimset = [NSCharacterSet characterSetWithCharactersInString:@"/"];
            NSString* uid = [url.path stringByTrimmingCharactersInSet: trimset];
            NSLog(@"chat with UID: %@", uid);
            XTPeerChatViewController *vc = [[XTPeerChatViewController alloc] initWithUID: uid];
            [self.navigationController pushViewController:vc animated:YES];
        } else if ([url.host isEqual:@"setting"]) {
            XTSettingViewController *vc = [[XTSettingViewController alloc] initWithStyle:UITableViewStyleGrouped];
//            SettingsViewController *vc = [[SettingsViewController alloc] initWithStyle:UITableViewStyleGrouped];

            [self.navigationController pushViewController:vc animated:YES];
        } else if ([url.host isEqual:@"uploadavator"] || [url.host isEqual:@"uploadavatar"]) {
            [self xtecherUploadAvatar];
        } else {
            NSLog(@"xtecherCommand: %@", url);
            [self xtecherCommand:url];
        }
        
        return NO;
    }
    return YES;
}

- (void) xtecherUploadAvatar {
    NSLog(@"xtecherUploadAvatar");
}

- (void) xtecherCommand: (NSURL*) url {
    if ([url.host isEqualToString:@"tabbar"]) {
        CGFloat tabBarHeight = 0;
        if ([url.path isEqualToString:@"/hide"]) {
            self.tabBarController.tabBar.hidden = YES;
        } else {
            self.tabBarController.tabBar.hidden = NO;
             tabBarHeight = _tabBarHeight;
        }
        [UIView animateWithDuration:1 animations:^{
            self.webView.scrollView.contentInset = UIEdgeInsetsMake(0, 0, tabBarHeight, 0);
            self.webView.scrollView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 0, tabBarHeight, 0);
        }];
    } else if ([url.host isEqualToString:@"openurl"]) {
        NSURL* url = [NSURL URLWithString:[@"http:/" stringByAppendingString:url.path]];
        if (url) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}

- (BOOL) shouldStartLoad: (NSURL*) url {
    NSLog(@"shouldStartLoad: %@", url);
    return YES;
}
@end
